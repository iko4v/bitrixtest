// Install Composer

[1] local/php_interface: create composer.json
[2] In terminal: cd local\php_interface
[3] Use command: composer require symfony/var-dumper
[4] Add local\php_interface\vendor to .gitignore (if need)
[5] Add to local\php_interface\init.php: require_once __DIR__ . '/vendor/autoload.php';