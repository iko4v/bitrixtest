<?php

namespace lib;

use CIBlock;

class Config
{
	public function __construct()
	{
		\CModule::IncludeModule('iblock');
	}

	public function getIdForCode(string $code): ?int
	{
		$responceBlock = CIBlock::GetList([], ['=CODE' => $code], false);
		if ($result = $responceBlock->fetch()) {
			return $result['ID'];
		}
		return null;
	}
}

?>